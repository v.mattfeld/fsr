# FSR Website Notes

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## TODO

- [ ] markdown parser
  - [ ] tags parsen zum sortieren der Pages in der Nav
    - [ ] ggf. die Nav vom Fullscreen nehmen?
  - [ ] GraphQL nahmen
- [x] CMS?
  - [x] Git nutzen?

## Website

- Header
- Navigation
  - Home
    - Wer sind wir
    - Termine
    - Mitmachen
    - Aktuelles
  - Fachschaft
    - FSR
      - Mitglieder
    - FSP
    - Gremien
    - Protokolle
  - Fachgruppen
    - Datascience
      - Studium/Infos
    - Informatik
      - Studium/Infos
    - Mathematik
      - Studium/Infos
  - OPhase
  - Events
    - Uebersicht
    - Ophase
    - sIT
    - ShietSheet
    - Archiv
  - Kontakt
    - Email/Signatur
  - Impressum
- Footer

  - Social Media
  - Kontakt
  - Impressum
  - Git
  - sIT

- [ ] Parsing md-files and images from a dedicated directory
  - [ ] git data source
- [ ] Facebook-Event Parsing
- [ ] Code formatter
  - [ ] Prettier?
- [ ] refactor
