module.exports = {
  siteMetadata: {
    title: `Fachschaft Mathematik, Informatik und Data Science`,
    description: `Georg-August-Universität Göttingen`,
    author: `Valerius Mattfeld`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data` // GraphQL file system root
      }
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: true,
        stripMetadata: true,
        defaultQuality: 75
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `FSR Website`,
        short_name: `fsrpage`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/data/images/logo.png` // This path is relative to the root of the site.
      }
    },
    {
      resolve: `gatsby-source-git`,
      options: {
        name: `protokolle-repo`,
        remote: `https://gitlab.gwdg.de/GAUMI-fachschaft/Protokolle.git`,
        patterns: `*_Protokolle/*.pdf`
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // CommonMark mode (default: true)
        // commonmark: true,
        // Footnotes mode (default: true)
        // footnotes: true,
        // Pedantic mode (default: true)
        // pedantic: true,
        // GitHub Flavored Markdown mode (default: true)
        gfm: true,
        // Plugins configs
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1024
            }
          }
        ]
      }
    }
  ]
};
