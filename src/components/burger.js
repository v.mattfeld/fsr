import React from "react";
import { push as Menu } from "react-burger-menu";
import { graphql, Link, useStaticQuery } from "gatsby";

export default props => {
  const navQuery = graphql`
    query SiteLinkData {
      allMarkdownRemark(sort: { fields: [frontmatter___title], order: ASC }) {
        group(field: frontmatter___tags) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                tags
                title
              }
            }
          }
        }
      }
    }
  `;

  const data = useStaticQuery(navQuery);
  const core = data.allMarkdownRemark.group[0].edges;
  const meta = data.allMarkdownRemark.group[1].edges;

  return (
    <Menu {...props} left width={"250px"} height={"150vh"} noOverlay>
      {meta.map(({ node }) => (
        <Link to={node.fields.slug}>{node.frontmatter.title}</Link>
      ))}
      <hr style={{ marginBottom: `15px` }} />
      {core.map(({ node }) => (
        <Link to={node.fields.slug}>{node.frontmatter.title}</Link>
      ))}
    </Menu>
  );
};
