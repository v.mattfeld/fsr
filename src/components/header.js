import React from "react";
import Img from "gatsby-image";
import { useStaticQuery, graphql } from "gatsby";

const Header = () => {
  const dynLogo = useStaticQuery(graphql`
    query DynLogoQuery {
      file(relativePath: { eq: "images/logo.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  return (
    <Img
      fluid={dynLogo.file.childImageSharp.fluid}
      alt="Fachschaft Mathematik u. Informatik"
      style={{ minWidth: `150px`, maxWidth: `200px` }}
    />
  );
};

export default Header;
