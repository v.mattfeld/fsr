import React from "react";
import { Link } from "gatsby";
import Header from "./header";
import Burger from "./burger";
import Navbar from "./navigation";
import "../styles/sass/style.scss";

export default ({ children }) => (
  <div id="App">
    <div id="burger">
      <Burger pageWrapId={"page-wrap"} outerContainerId={"App"} />
    </div>
    <div id="page-wrap" style={{ padding: `1.5rem` }}>
      <header>
        <Link to="/">
          <Header />
        </Link>
        <Navbar />
      </header>
      <div>{children}</div>

      <footer>
        © {new Date().getFullYear()}, Valerius Mattfeld (FSR 2019).{" "}
        <Link to="/impressum/">Impressum.</Link>
      </footer>
    </div>
  </div>
);
