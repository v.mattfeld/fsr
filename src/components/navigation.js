import React from "react";
import { Link, graphql, useStaticQuery } from "gatsby";
const ListLink = props => (
  <li>
    <Link className="navbarItem" to={props.to}>
      {props.children}
    </Link>
  </li>
);

const Navbar = () => {
  const data = useStaticQuery(graphql`
    query FullscreenNavbarQuery {
      allMarkdownRemark(
        filter: { frontmatter: { tags: { in: "nav" } } }
        sort: { fields: frontmatter___position, order: ASC }
      ) {
        edges {
          node {
            frontmatter {
              title
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `);
  return (
    <ul className="navbar">
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <ListLink to={node.fields.slug}>{node.frontmatter.title}</ListLink>
      ))}
    </ul>
  );
};

export default Navbar;
