---
title: "Kontakt"
date: "2019-08-25"
tags: ["meta", "nav"]
position: "6"
---

FSR Mathematik und Informatik Göttingen

Fachschaftsrat Mathematik und Informatik
<br>
Bunsenstraße 3-5
<br>
37073 Göttingen

<a href="mailto:fsr@math-cs.uni-goettingen.de">fsr@math-cs.uni-goettingen.de</a>
