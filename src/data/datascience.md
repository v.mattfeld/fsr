---
title: "Data Science"
date: "2019-08-12"
tags: ["core", "nav"]
position: "3"
---

Seit Februar 2018 hat die Fachgruppe Data Science ihren aktuellen Fachgruppensprecher Achim von Prittwitz.

Erreichen könnt ihr die Fachgruppe Data Science über die [FSR-Email](/contact/) und ab demnächst auch über eine eigene Fachgruppen-Mail.

Weitere Infos folgen!
