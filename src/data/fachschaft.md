---
title: "Fachschaft"
date: "2019-08-12"
tags: ["core", "nav"]
position: "0"
---

### Fachschaftsrat

#### Wie erreiche ich euch?

Den Fachschaftsraum findest du im Keller des Mathematischen Instituts, Bunsenstraße 3-5, neben der Cafete.
Du kannst uns natürlich auch gerne [eine E-mail schreiben](/contact/).

Eine Liste der aktuell gewählten FSR Mitglieder findest du [hier](/mitglieder/).

### Was ist der Fachschaftsrat?

Der Fachschaftsrat vertritt hochschulinterne sowie hochschulexterne Interessen der Studierenden eines Fachbereichs, d.h. er tritt für die Rechte und Forderungen der Studierendenschaft seines Fachbereichs sowohl innerhalb als auch außerhalb des Universitätsgeschehens ein. In ihm sitzen eine bestimmte (aber nicht zwangsläufig determinierte) Anzahl vom Fachschaftsparlament gewählter studentischer Vertreter.

### Was sind die Aufgaben des Fachschaftsrats genau?

Der Fachschaftsrat vertritt, wie oben bereits erwähnt, allgemein die Interessen der Studierenden unseres Fachbereichs – also der gesamten Fachschaft – bei der Wahrnehmung demokratischer Rechte gegenüber dem Ministerium, dem Dekanat, den Professoren und den anderen Organen der Studentenschaft, sowie dem ASTA. Dies bewirken wir unter anderem dadurch, dass in den meisten Ausschüssen und Gremien des Fachbereichs Vertreter des Fachschaftsrates sitzen und stimmberechtigt sind. Dadurch ist es uns möglich aktiv Einfluss auf viele Entscheidungen, welche die Studierenden betreffen, zu nehmen. Besonders wichtig ist an dieser Stelle das Mitspracherecht in der Studienkomission (StuKo).
Zusätzlich sind in den Berufungskommissionen Fachschaftsratsmitglieder und/oder Studierende vertreten, sodass wir direkten Einfluss auf die Auswahl der im Fachbereich dozierenden Professoren haben.
Außerdem wollen wir euch über alle die Fachschaft betreffenden Angelegenheiten informieren. Dies geschieht über die folgenden Medien:

1. Homepage (sollte euch gerade wohl klar sein)
1. Aushänge neben dem Fachschaftsraum
1. Ein abonnierbarer Newsletter

Dies ist aber noch nicht alles. Der Fachschaftsrat organisiert in Zusammenarbeit mit allen Studierenden Studienhilfen wie z.B. Klausursammlungen, welche ihr im Fachschaftsraum in einem Gleichnahmigen Ordner findet, Skripte, Beschaffung von Arbeitsmaterialien, pp.
Ebenfalls werden von uns auch Partys und Veranstaltungen, wie unser alljährliches Pokerturnier, geplant und durchgeführt.

### Fachschaftsratssitzung

Unsere Sitzungen sind öffentlich, d.h. jeder darf teilnehmen. Stimmberechtigt sind jedoch nur Mitglieder des Fachschaftsrates. Schaut doch einfach mal vorbei. Wir freuen uns über jeden Interessenten.
Die folgenden Termine finden im WiSe18/19 jeweils um 18 Uhr statt am:

- Mo, 15.04.2019, Institut für Informatik
- Mo, 29.04.2019, Mathematisches Institut
- Mo, 13.05.2019, Institut für Informatik
- Mo, 27.05.2019, Mathematisches Institut
- Mi, 13.06.2019, Institut für Informatik
- Mo, 24.06.2019, Mathematisches Institut
- Mo, 08.07.2019, Institut für Informatik

*IfI = Institut für Informatik, Seminarraum 1.101, dritter Stock direkt links
*MI = Mathematisches Institut, HS1

## Mitglieder

Beratend wären auch zusätzlich die FachgruppensprecherInnen dabei – wenn sie nicht sowieso dem FSR angehören würden: **Lorenz Glißmann (Informatik), Helen Gold (Mathematik)**.

[Mitglieder des Fachschaftsrates 2019/20](/mitglieder/)

## Fachschaftsparlament

### Wie erreiche ich euch?

Das Fachschaftsparlament erreichst du am besten per E-Mail: fsp@math-cs.uni-goettingen.de

### Was ist das Fachschaftsparlament?

In aller erster Linie wählt das Fachschaftsparlament den Fachschaftsrat und legt somit die Agenda der Legislaturperiode fest. Es führt Aufsicht und gibt Weisungen an den Fachschaftsrat.
Das Fachschaftsparlament ist direkt von den Studierenden der Fakultät legitimiert.

Das Fachschaftsparlament wird vertreten durch:

- Sergio Perez (Präsident),
- Janina Schmidt (stellv. Präsidentin)

### Was sind die Aufgaben des Fachschaftsparlament genau?

Wie bereits oben erwähnt, wählt das Fachschaftsparlament den Fachschaftsrat. Es erlässt den Haushalt, gründet oder verändert Fachgruppen, erteilt dem Fachschaftsrat Weisungen, kontrolliert die Einhaltung des Haushaltes und kann selbsttätig in Form von Kommissionen und Ausschüssen handeln.
Das Tagesgeschäft obliegt aber in den meisten Fällen dem Fachschaftsrat; daher kommt das Fachschaftsparlament in der Regel nur bei sehr wichtigen oder strittigen Themen zu Wort.

### Fachschaftsparlamentsitzungen

Sitzungen des Fachschaftsparlament sind unregelmäßig und werden hochschulöffentlich angekündigt, meist ist ein Aushang am Schwarzen Brett der Informatik zu finden. Weiterhin kann man sich auch auf einen Verteiler aufnehmen lassen.
Anträge an das Fachschaftsparlament sind per E-Mail zu stellen.

## Protokolle

Die Protokolle der Faschaftsrats- und Fachschaftsparlamentssitzungen stehen [hier zum download](/protokolle/) **(in Bearbeitung)** bereit oder sind auch in [unserem Git](https://gitlab.gwdg.de/GAUMI-fachschaft/Protokolle) auffindbar.
