---
title: "Home"
date: "2019-08-12"
tags: ["meta"]
---

Hier findest Du alle Informationen zum Fachschaftsrat Mathematik und Informatik der Georg August Universität Göttingen. Als Fachschaftsrat vertreten wir die Interessen der Studierenden der Mathematik und Informatik. Über alle wichtigen Termine und unsere Veranstaltungen wirst Du hier auf dem Laufen gehalten. Des Weiteren findest Du hier nützliche Links, die Dir den Einstieg in das Studium erleichtern sollen.

**Informationen zur [O-Phase gibt es hier](/ophase)**. Wenn du aufgrund von Arbeit oder anderen Dingen bereits jetzt dringend Informationen zum ersten Semester brauchst, melde dich gerne in einer der folgenden Gruppen oder schreib uns eine Email.

Wir freuen uns auf euch!
Euer FSR

<!-- Image wird transformiert in der index komponente geladen -->

![FSR 2019](./images/fsr2019_edit.jpg)
