---
title: "Informatik"
date: "2019-08-12"
tags: ["core", "nav"]
position: "2"
---

**Diese Seite wird momentan noch überarbeitet.**

Hallo, wir sind die **Fachgruppe Informatik**.
Solltest du uns kontaktieren wollen, kannst du uns über

<p style="text-align: center; font-size: 20px;">
    fachgruppe [at] informatik.uni-goettingen.de
</p>

erreichen.
