---
title: "Mathematik"
date: "2019-08-12"
tags: ["core", "nav"]
position: "1"
---

![Fachgruppe Mathematik](./images/fg_mathe.png)

Hallo!
Wir sind die Fachgruppe Mathematik und setzen uns für eure Interessen rund um das Mathe-Studium ein. Aber was heißt das überhaupt?

Zum Beispiel organisieren wir Info-Veranstaltungen, vertreten die Studierendenschaft in verschiedenen Gremien und sorgen für Abwechslung durch Events wie die Weihnachtsvorlesung und O-Phase.
Wenn euch das interessiert und ihr mitmachen wollt, oder ihr Probleme, Fragen und Anregungen habt, kommt gerne bei unseren offenen Sitzungen vorbei oder schreibt uns eine Mail.

Wir treffen uns jeden zweiten Montag um 18:00 Uhr im Fachschaftsraum (neben der Cafete) im Mathematischen Institut (MI). Folgende Termine sind:

- 17.06.19
- 01.07.19
- 15.07.19

Unsere aktuelle Fachgruppensprecherin heißt Helen Gold.

Erreichen könnt ihr uns unter

<center>
    fachgruppe@mathematik.uni-goettingen.de
</center>

Wir freuen uns, wenn ihr mal vorbeischaut!
