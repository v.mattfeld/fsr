---
title: "Mitglieder"
date: "2019-08-31"
---

Mitglieder des Fachschaftsrates 2019

<center>
<div style="margin-right: auto; margin-left: auto;">
<table>
<thead>
<tr>
<th>Referat</th>
<th>Person</th>
</tr>
</thead>
<tbody>
<tr>
<td>Vorsitz</td>
<td>Robin William Hundt</td>
</tr>
<tr>
<td>Finanzer</td>
<td>Christian Parchem</td>
</tr>
<tr>
<td>stellv. Sprecher</td>
<td>Lorenz Glißmann</td>
</tr>
<tr>
<td>stellv. Finanzer</td>
<td>Valentin Marr</td>
</tr>
<tr>
<td>2-Fach-Bachelor Mathematik</td>
<td>Jonas Mach</td>
</tr>
<tr>
<td>Fachgruppe Mathematik</td>
<td>Helen Carolin Gold</td>
</tr>
<tr>
<td>Fachgruppe Informatik</td>
<td>Lorenz Glißmann</td>
</tr>
<tr>
<td>Fachgruppe Data Science</td>
<td>Achim von Prittwitz</td>
</tr>
<tr>
<td>Ökologie</td>
<td>Felix Schelle</td>
</tr>
<tr>
<td>pol. Bildung &amp; Ethik I</td>
<td>Jannes Rösener</td>
</tr>
<tr>
<td>pol. Bildung &amp; Ethik II</td>
<td>Tim Oberländer</td>
</tr>
<tr>
<td>Fachschaftsvernetzung</td>
<td>Lorenz Glißmann</td>
</tr>
<tr>
<td>Sommerfest</td>
<td>Paul Würzberg</td>
</tr>
<tr>
<td>O-Phasenvorbereitungsseminar</td>
<td>Jannik Koch</td>
</tr>
<tr>
<td>Antidiskriminierung</td>
<td>Felix Schelle</td>
</tr>
<tr>
<td>Transparenz &amp; Öffentlichkeit</td>
<td>Valerius Mattfeld</td>
</tr>
<tr>
<td>Erstiheft</td>
<td>Felix Spühler</td>
</tr>
<tr>
<td>Internationales</td>
<td>Charlotte Ackva</td>
</tr>
<tr>
<td>Winterball</td>
<td>Jonas Hügel</td>
</tr>
</tbody>
</table>
</div>
</center>