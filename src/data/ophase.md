---
title: "O-Phase"
date: "2019-08-31"
tags: ["core", "nav"]
position: "3"
---

<!-- TODO https://www.gatsbyjs.org/packages/gatsby-remark-component/ fuer PDF Component und mobile view download link -->

**Achtung: Die Zeitpläne sind noch vom letzten Jahr und werden demnächst aktualisiert!**

Im Allgemeinen ist der Ablauf aber ähnlich zum letzten Jahr.

## Wann findet die O-Phase statt?

Dieses Jahr findet die O-Phase in der Woche vom **14. bis zum 17. Oktober 2019**.

Zur Erstsemestervernetzung gibt es jetzt ein paar [Messenger-Gruppen, die ihr hier findet](https://pad.gwdg.de/s/r1Rx2nY7H#). Darüber könnt ihr auch Fragen stellen und schonmal jetzt gucken mit wem ihr so zusammen studiert.

## Was verbirgt sich hinter der O-Phase?

Damit du einen guten Start ins Studium hast, organisieren der Fachschaftsrat (FSR) und die beiden Fachgruppen (FGs) zusammen mit vielen freiwilligen Helfern jedes Jahr für alle Erstsemester die sogenannte Orientierungsphase, kurz O-Phase. Wir zeigen dir welche Orte du in Göttingen kennen musst, was du Universität alles zu bieten hat und versorgen dich mit wichtigen Informationen über dein zukünftiges Studium.

Die Hauptsache haben wir verschwiegen: Du lernst deine Kommilitonen kennen! Tagsüber während des Programms hast du dazu reichlich Zeit, aber wer alles mit dir studiert, findest du am besten abends bei einem (Bier|Saft|Cocktail|Wasser) in der Kneipe oder auf den zahlreichen Ersti-Partys heraus.

Wir wünschen dir einen guten Start in Göttingen und eine unvergessliche O-Phase!

Bei Fragen kannst du dich per [E-mail](/contact/) an uns wenden.

## Wohin gehe ich, wenn ich Mathematik studiere?

Wir treffen uns erstmals am Montag um **10.00 Uhr im [Maximum des Mathematischen Instituts](https://lageplan.uni-goettingen.de/?ident=8228_1_1.OG_1.101).**

### Zeitplan

<iframe src="https://fsr.math-cs.uni-goettingen.de/wp-content/uploads/2018/09/timetable_math_2018.pdf" width="100%" height="500px"></iframe>

Bei Fragen kannst du dich per [Mail](/mathe/) bei uns melden.

## Wohin gehe ich, wenn ich Informatik studiere?

Treffen am Montag um [**9.00 Uhr in MN08**](https://www.geodata.uni-goettingen.de/geoplan/?ident=2409_1_EG_0.104) am Nordcampus.

Wenn du Fragen dazu hast, schreib einfach eine Mail an fachgruppe 0x40 informatik.uni-goettingen.de. ( Verstehst du nicht? hier nachschlagen ) Insbesondere, wenn du bereits vor der O-Phase aus bestimmten Gründen Informationen über deine Veranstaltungen im ersten Semester brauchst, melde dich gerne.

### Zeitplan

<iframe src="https://fsr.math-cs.uni-goettingen.de/wp-content/uploads/2018/09/timetable_info_2018.pdf" width="100%" height="500px"></iframe>

## Wohin gehe ich, wenn ich Data Science studiere?

Wir treffen uns erstmals am **[Montag um 10.00 Uhr im Institut für Informatik](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101)** . Der genaue Raum wird vom Haupteingang aus ausgeschildert sein.

Bei Fragen kannst du dich [per Mail](/datascience/) bei uns melden.

### Zeitplan

<iframe src="https://fsr.math-cs.uni-goettingen.de/wp-content/uploads/2018/09/timetable_data_2018.pdf" width="100%" height="500px"></iframe>

## Wohin gehe ich, wenn ich 2-Fach-Bachelor bin?

Die O-Phase ist so gestaltet, dass du auf jeden Fall alle wichtigen Veranstaltungen in beiden Fächern besuchen kannst. Für alle Lehramtsstudenten am Dienstag (**10. Oktober**) der O-Phase findet zu den weiteren Veranstaltungen noch die „**Zentrale Information für Lehramt**“ von **8.30 Uhr bis 13.00 Uhr** statt.

Am nächsten Tag, dem Mittwoch der O-Phase, ist ganztägig „Crossover-Tag“, an dem Veranstaltungen deines zweiten Faches an der jeweiligen Fakultät stattfinden (Wann? und Wo? findest du auf der jeweiligen Homepage). Falls du jedoch am Montag nicht an der Studienberatung bei uns teilnehmen kannst, melde dich einfach während der O-Phase bei uns, denn am Mittwoch Vormittag findet auch eine Studienberatung für alle 2-Fächer-Bachelor statt, die am Montag nicht zu der Studienberatung kommen können. Damit kannst du sicher sein, dass du keine wichtige Information verpasst.

Solltest du Mathematik und Informatik im 2-Fach-Bachelor studieren, kannst du dir aussuchen, zu welcher O-Phase du gehst. Viele Aktionen, wie zum Beispiel die Stadtrallye oder die O-Phasen-Abschlussparty finden gemeinsam statt. Aufgrund der viel geringeren Anzahl an Informatik-Lehrämtlern empfehlen wir Lehramtsstudierenden mit dieser Fächerkombination jedoch, am Montag erst einmal in die Informatik zu gehen.
