import React from "react";
import Layout from "../components/layout";
import { graphql } from "gatsby";

const Main = ({ data }) => {
  return (
    <Layout>
      <div>
        <h1>Willkommen!</h1>
      </div>
      <div dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }} />
    </Layout>
  );
};

export const query = graphql`
  query home {
    markdownRemark(frontmatter: { title: { eq: "Home" } }) {
      frontmatter {
        title
      }
      html
    }
  }
`;
export default Main;
