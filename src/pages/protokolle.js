import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';


export default () => {

    const data = useStaticQuery(graphql`
        query ProtokolleQuery {
            allFile(filter: {sourceInstanceName: {eq: "protokolle-repo"}}, sort: {fields: name}) {
                edges {
                    node {
                        publicURL
                        name
                    }
                }
            }
        }
    `)


    return (
        <div>
            <h1>Work in Progress!</h1>
            <ul>
                {data.allFile.edges.map(({ node }) => (
                    <li>
                        <Link to={node.publicURL}>{node.name}</Link>
                    </li>
                ))}
            </ul>
        </div>
    )
}